FROM php:7.4.33-fpm-alpine3.16
# 从官方拉去指定版本的基础镜像：https://hub.docker.com/_/php

# 修改镜像源，安装一些必备的软件包
RUN set -x \
  && sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories \
  && apk update \
  && apk add --no-cache tzdata vim nginx supervisor git \
  && cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
  && apk del tzdata

# 添加 php扩展docker安装工具，支持的扩展还蛮多的 https://github.com/mlocati/docker-php-extension-installer
ADD install-php-extensions /usr/local/bin/
# 添加要安装的扩展
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions gd bcmath ctype curl dom fileinfo json mbstring openssl PDO pdo_mysql redis Reflection tokenizer xml zip zlib
# 安装composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

RUN mkdir -p /var/log/supervisor && mkdir -p /etc/supervisor/conf.d/
COPY ./nginx.conf /etc/nginx/http.d/nginx.conf
COPY ./php.ini /usr/local/etc/php/php.ini
COPY ./supervisord.conf /etc/supervisor/conf.d/supervisord.conf

WORKDIR /var/www/html

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
